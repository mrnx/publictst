#include <iostream>
#include "Shape.h"
using namespace std;

struct Node {
	Shape shape;
	struct Node* next;
};

struct Node* head = NULL;
struct Node* tail = NULL;

void insert(Shape shape) {
	cout << "get into insert now:\n";
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));
	new_node->shape = shape;
	new_node->next = NULL;
	if (head == NULL) {
		head = new_node;
		
	}
	else {
		tail->next = new_node;
		tail = new_node;
	}
	
}	
void display() {
	struct Node* ptr;
	ptr = head;
	cout << "ptr is: " << ptr;
	while (ptr != NULL) {
		ptr->shape.print();  // when the print of Shape class is virtual, i get Read Access violation, when it's just void, programm runs. (??)
							 
		cout << (ptr->next!=NULL?",":"") << "..\n";
		ptr = ptr->next;
	}
}
