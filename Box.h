#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

class Box : public Shape {
private:
	int x, y, z;

public:
	Box() {
		cout << "Default Box constructor!!!\n";
	}

	 Box(int x, int y, int z) {
		 cout << "Box constructor2 for x,y,z = " << x << "," << y << "," << z << " \n";
		this->x = x;
		this->y = y;
		this->z = z;
	}

	void print() {
		cout << "\tBox(" << x << "," << y << "," << z << ')';
	}
};

