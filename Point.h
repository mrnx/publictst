#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

class Point : public Shape{
private:
	int x = 0, y = 0;
	
	//Shape ar[2];
public:
	Point() {
		cout << "Default Point constructor!!!\n";
		x = 0;
		y = 0;
	}

	Point (int a, int b) {
		x = a;
		y = b;
		cout <<  "Point constructor2 for x,y = "<< a << ','<< b << " \n";
	}
	

	 void print() {
		cout << "\tPoint(" << "" << "," << "" << ')';
	}
};

